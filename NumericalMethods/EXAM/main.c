#include"LU.h"

//printf square matrix
void printsm(gsl_matrix * M){
  size_t n=M->size1;

  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      double m=gsl_matrix_get(M,i,j);
      printf("%lg ",m);
    }
    printf("\n");
  }
}

void createH(gsl_matrix * H){
  size_t n=H->size1;
  for(int i=0; i<n; i++){ /* making upper triangular parts*/
    for(int j=i; j<n; j++){
      double r = rand() %9; /*random number between 0 and 9*/
      r=r+1;
      gsl_matrix_set(H,i,j,r);
    }
  }

  for(int i=0; i<n-1; i++){ /*making the subdiagonal*/
    double r = rand() %9;
    r=r+1;
    gsl_matrix_set(H,i+1,i,r);
  }
}

int main(){
  int n=5;
  gsl_matrix * H = gsl_matrix_alloc(n,n);
  gsl_matrix * L = gsl_matrix_alloc(n,n);
  gsl_matrix * U = gsl_matrix_alloc(n,n);
  gsl_matrix * R = gsl_matrix_alloc(n,n);

  createH(H);

  printf("#LU-decompostion of Hessenberg matrix, H= LU\n");
  printf("H= \n");
  printsm(H);
  printf("\n \n");

  LU_decomp(H,L,U);

  printf("After decomposition:\n");
  printf("L = \n");
  printsm(L);
  printf("\n \n");
  printf("U = \n");
  printsm(U);

  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,L,U,0.0,R);
  printf("\n \n");
  printf("Checking that the algorithm works: LU = (should be H)\n");
  printsm(R);

  printf("\n \n");
  printf("#Now, we want to check that the number of operation O(n^2).\n");
  printf("#n O\n");

  int N[] = {1, 5, 10, 15, 20, 40, 70, 100};
  size_t I = sizeof(N)/sizeof(N[0]);

  for(int i=0; i<I;i++){
    int n=N[i];
    gsl_matrix * H = gsl_matrix_alloc(n,n);
    gsl_matrix * L = gsl_matrix_alloc(n,n);
    gsl_matrix * U = gsl_matrix_alloc(n,n);

    createH(H);
    int O = LU_decomp(H,L,U);
    printf("%i %i\n",n,O);

    gsl_matrix_free(H);
    gsl_matrix_free(L);
    gsl_matrix_free(U);
  }

  gsl_matrix_free(H);
  gsl_matrix_free(L);
  gsl_matrix_free(U);
  gsl_matrix_free(R);
  return 0;

}
