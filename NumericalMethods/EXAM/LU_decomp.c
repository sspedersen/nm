#include"LU.h"

int LU_decomp(gsl_matrix * H, gsl_matrix * L, gsl_matrix * U){
int n=H->size1;
int op=0;

gsl_matrix_set_identity(L);

for(int i=0; i<n; i++){
  op++;
  gsl_matrix_set(U,0,i,gsl_matrix_get(H,0,i));
}

for(int i=0; i<n-1; i++){
  op++;
  double li= gsl_matrix_get(H,i+1,i)/gsl_matrix_get(U,i,i);
  gsl_matrix_set(L,i+1,i,li);

  for(int j=1; j<n; j++){
    op++;
    double u = gsl_matrix_get(H,i+1,j) - li*gsl_matrix_get(U,i,j); /*ui+1,j = hi+1,j - li*ui,j */
    gsl_matrix_set(U,i+1,j,u);
  }
}

return op;
}
