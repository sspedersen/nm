#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.H>

int LU_decomp(gsl_matrix * H, gsl_matrix * L, gsl_matrix * U);
