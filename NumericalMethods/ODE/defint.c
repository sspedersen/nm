#include"ode.h"

double defint(
	double a,
	double b,
	double h,
	gsl_vector * y,
	double abs,
	double eps,
	void f(double x, gsl_vector *y, gsl_vector *dydx))
{
	driver(&a,b,&h,y,abs,eps,&rkstep12,f);
	double I = gsl_vector_get(y,0);
	return I;

}
