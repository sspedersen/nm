#include"ode.h"

int main(){
  int n=2;
  double dx=0.05, x=0;
  // Part A;
  gsl_vector * yx= gsl_vector_alloc(n);
  gsl_vector * yxplus = gsl_vector_alloc(n);
  gsl_vector * err = gsl_vector_alloc(n);

  //function:
  void func(double x,gsl_vector*y,gsl_vector*dydx){
    assert(y->size==2);
    //harmonic oscillator: u''=-u; y1=u, y2=u'; y1'=y2, y2'=-y1;
    gsl_vector_set(dydx,0,gsl_vector_get(y,1));
    gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
  }
  // initial conditions
  gsl_vector_set(yx,0,1);
  gsl_vector_set(yx,1,0);
  printf("#TESTING ODE12 FOR HARMONIC OSCIALLATOR:\n");
  printf("#Initial conditions: %g %g %g\n",x,gsl_vector_get(yx,0),gsl_vector_get(yx,1));
  printf("#x dydx dydx_2 err1 err1\n ");

  for(;x<2*M_PI; x+=dx){
    rkstep12(x,dx,yx,func,yxplus,err);
    gsl_vector_memcpy(yx,yxplus);
    printf("%g %g %g %g %g\n",x,gsl_vector_get(yx,0),gsl_vector_get(yx,1), gsl_vector_get(err,0),gsl_vector_get(err,1));
  }
  //A2
  double a = 0, b = 2, h, abs = 1e-6, eps = 1e-6;

	gsl_vector_set(yx,0,1);
	gsl_vector_set(yx,1,0);
	h = copysign(0.1,b-a);
	driver(&a,b,&h,yx,abs,eps,&rkstep12,&func);

	printf("\n#TESTING DRIVER FOR HARMONIC OSCILLATOR, a = 0, b = 2:\n");
	printf("#y'(%g) = %g y(%g) = %g\n",a,gsl_vector_get(yx,0),a,gsl_vector_get(yx,1));
	printf("#Exact solution is %g %g\n",cos(a), -sin(a));

  // Part B; path storing
  n = 2;
	int maxSteps = 1e5, DRIVER_FAIL=0;
	a=0; b=M_PI;

	gsl_vector * tPath = gsl_vector_alloc(maxSteps);
	gsl_matrix * yPath = gsl_matrix_alloc(n,maxSteps);

	gsl_vector_set(tPath,0,a);
	gsl_matrix_set(yPath,0,0,1);
	gsl_matrix_set(yPath,0,1,0);
	h = copysign(0.01,b-a);

	int lastStep = driverPathStoring(tPath,b,&h,yPath,abs,eps,&rkstep12,&func);
  //fprintf(stderr,"lastStep = %i\n",lastStep);
	if( lastStep == DRIVER_FAIL )
	{
		fprintf(stderr,"Driver couldn't find a solution within %d steps\n",maxSteps);
		return 0;
	}

  printf("\n \n");
	printf("#TESTING PATHSTORING ON HARMONIC OSCIALLTOR:\n");
	printf("# t \t yODE \t yExact\n");
	for(int i = 0; i < lastStep; i++)
	{
		printf("%g %g %g\n",gsl_vector_get(tPath,i),gsl_matrix_get(yPath,0,i),gsl_matrix_get(yPath,1,i));
	}

  printf("\n \n");
  printf("#TESTING THE DEFINITE INTEGRAL:\n");

  void interesting(double x, gsl_vector * y, gsl_vector * dydx){
    assert(y->size==1);
    gsl_vector_set(dydx,0,pow(x-x*x,4)/(1+x*x));

  }

  void interesting2(double x, gsl_vector* y, gsl_vector* dydx){
    assert(y->size==1);
    gsl_vector_set(dydx,0,x);

  }

  a = 0, b = 1, abs = 1e-6, eps = 1e-6;
	gsl_vector * y = gsl_vector_alloc(1);
	gsl_vector_set(y,0,a);
	h = copysign(0.01,b-a);

  double I=defint(a,b,h,y,abs,eps,&interesting);
  printf("f(x) = (x-x*x)^4/(1+x*x) from 0 to 1 \n");
  printf("I = %g\n",I);
  printf("Exact Solution: I = %g\n",0.001264489);
  printf("\n \n");

  a=-5, b=5, h=copysign(0.1,b-a);
  gsl_vector_set(y,0,-5);
  I=defint(a,b,h,y,abs,eps,&interesting2);
  printf("f(x) = x from -5 to 5 \n");
  printf("I = %g\n",I);
  printf("Exact Solution: I = 0\n");

  return 0;
}
