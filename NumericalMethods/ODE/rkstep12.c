#include"ode.h"

void rkstep12(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydx),
	gsl_vector * yx_plus_h, gsl_vector * err
)
{
	int n = yx -> size;
	gsl_vector * k = gsl_vector_alloc(n);
	gsl_vector * k0 = gsl_vector_alloc(n);
	gsl_vector * k1 = gsl_vector_alloc(n);
	gsl_vector * temp = gsl_vector_alloc(n);
	f(x,yx,k0);
	gsl_vector_memcpy(temp,k0);
	gsl_vector_scale(temp,h);// temp=h*k0
	gsl_vector_add(temp,yx); // temp = yx + h*k0
	f(x+h,temp,k1);
	gsl_vector_add(k1,k0);
	gsl_vector_scale(k1,0.5);
	gsl_vector_memcpy(k,k1);


	for (int i = 0; i < yx->size; i++)
	{
		double y=gsl_vector_get(yx,i);
		double yx_plus_h_star=gsl_vector_get(yx,i)+h*gsl_vector_get(k0,i);
		gsl_vector_set(yx_plus_h,i,y+h*gsl_vector_get(k,i));
		gsl_vector_set(err,i,gsl_vector_get(yx_plus_h,i)-yx_plus_h_star);
	}
	gsl_vector_free(k);
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(temp);

	//set yx_plus_h and err
}
