#include<omp.h>
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void plainmc_SMP ( int dim , double *a , double *b ,
              double f(double* x),int N, double *result , double *error);

void plainmc ( int dim , double *a , double *b ,
              double f(double* x),int N, double *result , double *error);

int main(){
	//testing plain mc for a phere with radius 1;
	int dim=3;
  double a[3]= {0, 0, 0};
  double b[3]= {1, M_PI, 2*M_PI};

  int N=1e5;
  double result, error;

  double f1(double *x){
    return sin(x[1])*x[0]*x[0]; }

  plainmc_SMP(dim,a,b,&f1,N,&result,&error);
  printf("A) TESTING PLAIN MC SMP: \n");
  printf("i) The Volume of a Sphere with radius=1: Integrating 4*pi*r^2 \n");
  printf("MC_result= %g\n",result);
  printf("Exact = %g\n",4.0/3.0*M_PI);
  printf("\n \n");

	dim=1;
  double a1[1]= {0};
  double b1[1]= {1};

  double f2(double *x){
    return 1/sqrt(x[0]);
  }

  plainmc_SMP(dim,a1,b1,&f2,N,&result,&error);
  printf("ii) Integrating 1/sqrt(x) \n");
  printf("MC_result= %g\n",result);
  printf("Exact = %g\n",2.0);

	printf("\n \n");

	double result_SMP, error_SMP;
	// PART B:
	int forloop=1e6;
	dim=3;
	printf("\n \n");
  printf("#B) Estimating error and result with regular and SMP Monte Carlo: \n");
	printf("# N error_SMP result_SMP error result\n");
	for (int i=10; i<forloop; i*=10) {
		plainmc_SMP(dim,a,b,&f1,i,&result_SMP,&error_SMP);
		plainmc(dim,a,b,&f1,i,&result,&error);
		printf("%i %g %g %g %g\n",i,error_SMP,result_SMP,error,result);


	}

	return 0;
}
