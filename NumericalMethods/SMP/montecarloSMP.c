#include <math.h>
#include<omp.h>
#include <stdlib.h>
#include<stdio.h>
#include <gsl/gsl_rng.h>
//#define RND ((double)rand_r()/RAND_MAX)

void randomx_SMP(int dim, double *a, double *b, double *x,gsl_rng* r) { for(int i=0;i<dim; i++) x[i]=a[i]+gsl_rng_uniform(r)*(b[i]-a[i]); }

void plainmc_SMP ( int dim , double *a , double *b ,
              double f(double* x),int N, double *result , double *error) {
  double V=1;
  for(int i=0;i<dim;i++) V *=b[i]-a[i];
  double sumfirst=0, sum2first=0, sumsecond=0, sum2second=0, fx1, fx2, x[dim];

  const gsl_rng_type *T;
  gsl_rng *r1,*r2;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r1 = gsl_rng_alloc(T);
	r2 = gsl_rng_alloc(T);

  #pragma omp parallel sections
  {
  #pragma omp sectin
  for(int i=0;i<N/2; i++){
    randomx_SMP(dim,a,b,x,r1); fx1=f(x);
    sumfirst+= fx1 ; sum2first += fx1*fx1 ;
  }
  #pragma omp section
  for(int i=N/2; i<N; i++){
    randomx_SMP(dim,a,b,x,r2); fx2=f(x);
    sumsecond += fx2 ; sum2second += fx2*fx2 ;
  }
  }

  double sum = sumfirst + sumsecond;
  double sum2 = sum2first + sum2second;
  double avr = sum/N;
  double var = sum2/N-avr*avr ;
  *result = avr*V;
  *error = sqrt(var/N)*V;

}
