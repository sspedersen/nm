#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<lineq.h>

int newtonmin(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
              gsl_vector* x, double dx, double eps){
  int n=x->size, count=0;
  gsl_matrix* H = gsl_matrix_alloc(n,n);
  gsl_matrix* Hplus=gsl_matrix_alloc(n,n);
  gsl_matrix* R = gsl_matrix_alloc(n,n);
  gsl_vector* df=gsl_vector_alloc(n);
  gsl_vector* dfplus=gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n);
  gsl_vector* xplus = gsl_vector_alloc(n);

  do {
    count++;
    double fx=f(x,df,H);
    qr_gs_decomp(H,R);
    qr_gs_solve(H,R,df,Dx);
    gsl_vector_scale(Dx,-1);
    //linesearch
    double s=1;
		while(1){
			count++;
			gsl_vector_memcpy(xplus,x);
			gsl_vector_add(xplus,Dx);
			double fxplus=f(xplus,dfplus,Hplus);
			if( fx <(1-s/2)*fxplus || s<0.02 ) break;
			s*=0.5;
			gsl_vector_scale(Dx,0.5);
      fx=fxplus;
			}
		gsl_vector_memcpy(x,xplus);
		gsl_vector_memcpy(df,dfplus);
		if( gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(df)<eps ) break;
  } while(1);


  gsl_vector_free(df);
  gsl_vector_free(dfplus);
  gsl_vector_free(Dx);
  gsl_vector_free(xplus);
  gsl_matrix_free(H);
  gsl_matrix_free(Hplus);
  gsl_matrix_free(R);

  return count;


}
