#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

int quasinewton
(double f(gsl_vector *x, gsl_vector *df),gsl_vector* x, double dx, double eps){
  int n=x->size;
  //gsl_vector *x = gsl_vector_alloc(n);
  gsl_vector *xplus=gsl_vector_alloc(n);
  gsl_vector *df = gsl_vector_alloc(n);
  gsl_vector *dfplus= gsl_vector_alloc(n);
  gsl_matrix * Hinv=gsl_matrix_alloc(n,n);
  //gsl_vector * s=gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n); //s=Dx
  gsl_vector* y=gsl_vector_alloc(n);
  gsl_vector* Hinvy=gsl_vector_alloc(n);
  gsl_vector* u= gsl_vector_alloc(n);
  gsl_vector* v=gsl_vector_alloc(n);
  gsl_vector* Hinvs=gsl_vector_alloc(n);
  gsl_vector* fxcopy=gsl_vector_alloc(n);



  gsl_matrix_set_identity(Hinv);
  /*delta x=-Hinv* gradf*/
  int count=0;

   double fx=f(x,df),fxplus;

do{
    count++;
    //determining delta x:
    gsl_blas_dgemv(CblasNoTrans,1.0,Hinv,df,0.0,Dx);
    gsl_vector_scale(Dx,-1.0);

    //and minimizing with lambda
    double lambda=1.0;
    int innercount=0;
while(1){/* linesearch */
	innercount++;
	gsl_vector_memcpy(xplus,x);
	gsl_vector_add(xplus,Dx);

	fxplus=f(xplus,dfplus);


	if( fxplus < (1-lambda/2)*fx || lambda<1.0/128)
	{
      		fx=fxplus;
		gsl_vector_memcpy(x,xplus);
		gsl_vector_memcpy(df,dfplus);
		break;
	}
	lambda*=0.5;
 	gsl_vector_scale(Dx,0.5);

    } /* linesearch */
    //calculating the correction to H, dH;
      gsl_vector_memcpy(y,dfplus);
      gsl_vector_sub(y,df); //y=dfplus-df
      gsl_blas_dgemv(CblasNoTrans,1.0,Hinv,y,0.0,Hinvy); //Hinv*y

      gsl_vector_memcpy(u,Dx);
      gsl_vector_sub(u,Hinvy); //u=Dx-Hinvy
	double uTy;
	gsl_blas_ddot(u,y,&uTy);
	double sTy;
	gsl_blas_ddot(Dx,y,&sTy);
//      gsl_blas_dger(1/uTy,u,u,Hinv); //Hinv=Hinv+u*uT/uTy;
if(fabs(sTy)>1e-6)
      gsl_blas_dger(1/sTy,u,Dx,Hinv);

if(gsl_blas_dnrm2(df)<eps){
	//printf("|df|<eps\n");
	break;
	}


}while(1);

   gsl_vector_free(xplus);
   gsl_vector_free(df);
   gsl_vector_free(dfplus);
   gsl_vector_free(Dx);
   gsl_vector_free(y);
   gsl_vector_free(Hinvy);
   gsl_vector_free(u);
   gsl_vector_free(v);
   gsl_vector_free(Hinvs);
   gsl_vector_free(fxcopy);
   gsl_matrix_free(Hinv);

   return count;
}
