#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int newtonmin(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
              gsl_vector* x, double eps);

int quasinewton(double f(gsl_vector *x, gsl_vector *df),gsl_vector* x,
                double dx, double eps);

int main(){
  double eps=1e-4;
  int ncallsr=0;
  int n=2;
  gsl_vector *x = gsl_vector_alloc(n);
  gsl_vector *df = gsl_vector_alloc(n);
  gsl_matrix * H=gsl_matrix_alloc(n,n);

/*first, the Rosenbrock function:*/
  double ros;
  double fros(gsl_vector* p,gsl_vector* df, gsl_matrix* H){
		ncallsr++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(df,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
		gsl_vector_set(df,1, 100*2*(y-x*x));
		gsl_matrix_set(H,0,0,2-400*y+1200*x*x);
		gsl_matrix_set(H,0,1,-400*x);
		gsl_matrix_set(H,1,0,-400*x);
		gsl_matrix_set(H,1,1,200);
    ros=(1-x)*(1-x)+100*(y-x*x)*(y-x*x);
    return ros;
  }

  printf("A) NEWTON'S METHOD\ni) ROSENBROCK'S FUNCTION:\n");
  gsl_vector_set(x,0,2);
  gsl_vector_set(x,1,4);
  fros(x,df,H);
  vector_print("initial vector x:",x);
  printf("function value f(x): %g\n \n",ros);
  int count=newtonmin(fros,x,eps);
  fros(x,df,H);
  vector_print("solution x: ",x);
  printf("function value f(x): %g\n \n",ros);
	printf("number of while loops: %i\n",count);
	printf("\n \n");



/* now, the Himmeblau function*/
double him;
double fhim(gsl_vector* p, gsl_vector* df, gsl_matrix* H){

		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
    gsl_vector_set(df,0,2*(x*x+y-11)*2*x+2*(x+y*y-7)    );
    gsl_vector_set(df,1,2*(x*x+y-11)    +2*(x+y*y-7)*2*y);
		gsl_matrix_set(H,0,0,4*(x*x+y-11)+8*x*x+2);
		gsl_matrix_set(H,0,1,4*x+4*y);
		gsl_matrix_set(H,1,0,4*x+4*y);
		gsl_matrix_set(H,1,1,4*(x+y*y-7)+8*y*y+2);

	double him=(x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);
  return him;
}

gsl_matrix * X=gsl_matrix_alloc(2,5); //allocating the 5 initial x's; based on wikipedia extrema
gsl_matrix_set(X,0,0,-1);
gsl_matrix_set(X,1,0,-1);
gsl_matrix_set(X,0,1,5);
gsl_matrix_set(X,1,1,5);
gsl_matrix_set(X,0,2,-3);
gsl_matrix_set(X,1,2,-3);
gsl_matrix_set(X,0,3,-3);
gsl_matrix_set(X,1,3,3);
gsl_matrix_set(X,0,4,3);
gsl_matrix_set(X,1,4,-2);

printf("ii) HIMMELBLAU'S FUNCTION:\n");
for(int i=0; i<5;i++){

	gsl_vector_set(x,0,gsl_matrix_get(X,0,i));
	gsl_vector_set(x,1,gsl_matrix_get(X,1,i));
	printf("Extremum %i:\n",i+1);
	vector_print("initial vector x:",x);
	him=fhim(x,df,H);
	printf("function value f(x): %g\n\n",him);
	count= newtonmin(fhim,x,eps);
	vector_print("solution x: ",x);
	him=fhim(x,df,H);
	printf("function value f(x): %g\n",him);
	printf("number of while loops: %i\n",count);
	printf("\n");
}

// B) quasinewtonian method;
//Rosenbrocks function:
double fros2(gsl_vector* p,gsl_vector* df){
	double a=1;
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(df,0, 2*(1-x)*(-1)+a*2*(y-x*x)*(-1)*2*x);
	gsl_vector_set(df,1, a*2*(y-x*x));
	double ros2=(1-x)*(1-x)+a*(y-x*x)*(y-x*x);
	return ros2;
}

double fhim2(gsl_vector* p, gsl_vector* df){

	double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
  gsl_vector_set(df,0,2*(x*x+y-11)*2*x+2*(x+y*y-7)    );
  gsl_vector_set(df,1,2*(x*x+y-11)    +2*(x+y*y-7)*2*y);
	double him=(x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);
  return him;
}

	printf("B) QUASINEWTON'S METHOD\ni) ROSENBROCK'S FUNCTION:\n");
	gsl_vector_set(x,0,1.5);
	gsl_vector_set(x,1,1.5);
  	ros=fros2(x,df);
	double dx=0.01;
  vector_print("initial vector x:",x);
  vector_print("initial gradient df:",df);
  printf("function value f(x): %g\n \n",ros);
  count=quasinewton(fros2,x,dx,eps);
  ros=fros2(x,df);
  vector_print("solution x: ",x);
  printf("function value f(x): %g\n \n",ros);
	printf("number of while loops: %i\n",count);
	printf("\n \n");


	printf("ii) HIMMELBLAU'S FUNCTION:\n");
	gsl_matrix_set(X,0,0,-1);
	gsl_matrix_set(X,1,0,-1);
	gsl_matrix_set(X,0,1,0);
	gsl_matrix_set(X,1,1,0);
	gsl_matrix_set(X,0,2,3);
	gsl_matrix_set(X,1,2,-2);
	gsl_matrix_set(X,0,3,-3);
	gsl_matrix_set(X,1,3,3);
	for(int i=0; i<4;i++){
		gsl_vector_set(x,0,gsl_matrix_get(X,0,i));
		gsl_vector_set(x,1,gsl_matrix_get(X,1,i));
		printf("Extremum %i:\n",i+1);
		vector_print("initial vector x:",x);
		him=fhim2(x,df);
		printf("function value f(x): %g\n\n",him);
		count= quasinewton(fhim2,x,dx,eps);
		vector_print("solution x: ",x);
		him=fhim2(x,df);
		printf("function value f(x): %g\n",him);
		printf("number of while loops: %i\n",count);
		printf("\n");
	}


	// B) fit data
	double master(pars, df){
		double fexp(pars,B){
			fexp=pars[0]*exp(-t/pars[2])+pars[3]; 
			return fexp;
		}
		double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
		double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
		double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
		int N = sizeof(t)/sizeof(t[0]);
		double F;
		for(int i=0; i<N; i++){
			F += pow(fexp(t[i]-y[i]),2)/(e(i)*e(i)) ;
		}

		return F;
	}



	gsl_matrix_free(H);
	gsl_vector_free(x);
	gsl_vector_free(df);
 return 0;

}
