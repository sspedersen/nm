#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int newtonmin(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
              gsl_vector* x, double eps);

int quasinewton(double f(gsl_vector *x, gsl_vector *df),gsl_vector* x,
                double dx, double eps);
