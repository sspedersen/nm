#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>

void rkstep12(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydx),
	gsl_vector * yx_plus_h, gsl_vector * err
);

void driver(
	double *t,
	double b,
	double *h,
	gsl_vector *y,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
);

int driverPathStoring(
	gsl_vector *tPath,
	double b,
	double *h,
	gsl_matrix *yPath,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
);

double defint(
	double a,
	double b,
	double h,
	gsl_vector * y,
	double abs,
	double eps,
	void f(double x, gsl_vector *y, gsl_vector *dydx));
