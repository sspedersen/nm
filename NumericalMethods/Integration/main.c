#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>

double adapt
	(double f(double),double a,double b,double acc,double eps);

double adaptinf(double f(double), double a, double b, double acc, double eps);

double clenshaw_curtis(double f(double),double a,double b,double acc,double eps);

double defint(
	double a,
	double b,
	double h,
	gsl_vector * y,
	double abs,
	double eps,
	void f(double x, gsl_vector *y, gsl_vector *dydx));

int main(){

  int calls=0;
	double a=0,b=1,acc=1e-6,eps=1e-4;;
	double s(double x){calls++; return sqrt(x);}; //nested function
	//double s(double x){calls++; return exp(-x/(1-x))*1/(1-x)/(1-x);} //nested function
	calls=0;
	double Q=adapt(s,a,b,acc,eps);
	double exact=2./3;
  printf("A) TESTING ADAPTIVE INTEGRATOR:\n");
	printf("integrating sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

  printf("\n \n");
  calls=0;
  double ss(double x){calls++; return 1.0/sqrt(x); };
  Q=adapt(ss,a,b,acc,eps);
  exact=2;
  printf("integrating 1/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

  printf("\n \n");

  calls=0;
  double sss(double x){calls++; return log(x)/sqrt(x);};
  Q=adapt(sss,a,b,acc,eps);
  exact=-4;
  printf("integrating ln(x)/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

  printf("\n \n");

  calls=0;
  acc=1e-6; eps=1e-6;
  double calc(double x){calls++; return 4*sqrt(1-(1-x)*(1-x));};
  Q=adapt(calc,a,b,acc,eps);
  exact=M_PI;
  printf("integrating 4*sqrt(1-(1-x)^2) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %.15g\n",Q);
	printf("          exact = %.15g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("\n \n");

	printf("B) TESTING ADAPTIVE INTEGRATOR WITH INFINITE LIMITS:\n");
	calls=0;
	double i(double x){
		calls++; return exp(-x);}
	a=0, b=INFINITY;
	//fprintf(stderr,"TEST: isinf(a)=%i isinf(b)=%i\n",isinf(a),isinf(b));
	Q=adaptinf(i,a,b,acc,eps);
	exact=1.0;
	printf("integrating exp(-x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %.15g\n",Q);
	printf("          exact = %.15g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("\n \n");

	calls=0;
	double ii(double x){calls++; return 1/(pow(exp(x)+x+1,2)+M_PI*M_PI); }
	a=-INFINITY, b=INFINITY;
	fprintf(stderr,"TEST: a=%g, b=%g, isinf(a)=%i isinf(b)=%i\n",a,b,isinf(a),isinf(b));
	Q=adaptinf(ii,a,b,acc,eps);
	exact=2.0/3.0;
	printf("integrating 1/((e^x+x+1)^2+pi^2) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %.15g\n",Q);
	printf("          exact = %.15g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	printf("\n\n");


	printf("C) The Clenshaw-Curtis vs. ODE Integrator:\n");
	double c(double x){return x; }

	a=-1, b=1;
	exact=0;
	double Q_CC=clenshaw_curtis(c,a,b,acc,eps);
// and with ODE
	gsl_vector * y = gsl_vector_alloc(1);
	gsl_vector *dydx = gsl_vector_alloc(1);

	void c_ODE(double x, gsl_vector *y, gsl_vector *dydx){gsl_vector_set(dydx,0,x); }

	a=-1, b=1;
	gsl_vector_set(y,0,0);
	double h=copysign(0.1,b-a);
	double Q_ODE;

	h=copysign(0.1,b-a);
	Q_ODE=defint(a,b,h,y,acc,eps,c_ODE);
	printf("integrating x from %g to %g\n",a,b);
	printf("           Q_CC = %g\n",Q_CC);
	printf("					Q_ODE = %g\n",Q_ODE);
	printf("          exact = %g\n",exact);


	gsl_vector_free(y);
	gsl_vector_free(dydx);
	return 0;
}
