#include"lineq.h"

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
  assert(Q->size1 == Q->size2);
  size_t N=Q->size1;

  gsl_vector *b= gsl_vector_calloc(N);
  gsl_vector *x= gsl_vector_calloc(N);

  for(int i=0; i<N; i++){
    gsl_vector_set(b,i,1.0);
    qr_gs_solve(Q,R,b,x);
    gsl_vector_set(b,i,0.0);
    gsl_matrix_set_col(B,i,x);
  }

  gsl_vector_free(b);
  gsl_vector_free(x);
}
