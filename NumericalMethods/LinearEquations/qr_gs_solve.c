#include"lineq.h"

void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){
//  assert(Q->size1 == Q -> size2);
assert(Q->size2==x->size);
assert(Q->size1==b->size);
  int m=Q->size2;
  gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); /*QT*b -> x*/

  for(int i=m-1; i>=0; i--){
    double s=0;
    for(int k=i+1; k<m; k++){
      s+= gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);

    }
    double ci=gsl_vector_get(x,i);
    double Rii=gsl_matrix_get(R,i,i);
    gsl_vector_set(x,i,(ci-s)/Rii);
  }

}
