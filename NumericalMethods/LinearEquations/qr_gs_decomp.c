#include"lineq.h"
#include<stdio.h>
#include<gsl/gsl_matrix.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
  int n=A->size1; int m=A->size2;
  /*fprintf(stdout,"n=%i m=%i\n",n,m);*/
  for (int sojle = 0; sojle < m ; sojle++) {
    double norm_ai=0;

    for(int r=0; r < n ; r++){
    double ars=gsl_matrix_get(A,r,sojle);
    norm_ai += ars*ars;
    }


  norm_ai=sqrt(norm_ai);

  gsl_matrix_set(R,sojle,sojle,norm_ai);

    for(int r=0; r < n; r++){
      double ars=gsl_matrix_get(A,r,sojle);
      double qr=ars/norm_ai;
      gsl_matrix_set(A,r,sojle,qr);

    }


    for(int j=sojle+1; j < m; j++){
      double qsaj=0;

      for(int k=0; k < n; k++){
        double qks=gsl_matrix_get(A,k,sojle);
        double akj=gsl_matrix_get(A,k,j);
        qsaj += qks*akj;
      }

      gsl_matrix_set(R,sojle,j,qsaj);

      for(int k=0; k < n ; k++){
        double akj=gsl_matrix_get(A,k,j);
        double qks=gsl_matrix_get(A,k,sojle);
        akj=akj-qsaj*qks;
        gsl_matrix_set(A,k,j,akj);
      }
    }

  }

}
