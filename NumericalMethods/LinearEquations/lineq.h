#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#include <assert.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix * Q, gsl_matrix * R, gsl_vector * b, gsl_vector *x);
void qr_gs_inverse(gsl_matrix * Q, gsl_matrix * R, gsl_matrix * B);
