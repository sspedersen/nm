#include"lineq.h"


int main(void){
size_t n=3; size_t m=2;
size_t N=3;
gsl_matrix * A= gsl_matrix_alloc (n,m);
gsl_matrix * R= gsl_matrix_alloc(m,m);
gsl_matrix * C= gsl_matrix_alloc(N,N); /*for the back substitution*/
gsl_matrix * RC= gsl_matrix_alloc(N,N);
gsl_vector * B= gsl_vector_alloc(N);
gsl_vector * X=gsl_vector_alloc(N);
gsl_matrix * INV=gsl_matrix_alloc(N,N);
gsl_matrix * D=gsl_matrix_alloc(N,N);
printf("First, we de the QR GS decomposition. \n");
printf("A=\n");

  for(int i=0; i<n; i++){
    for(int j=0; j<m; j++){
      double r = rand() %10; /*random double in range 0 to 9*/
      gsl_matrix_set(A,i,j,r);
      printf("%lg ",r);
    }
    printf("\n");
  }

printf("\n \n");

qr_gs_decomp(A,R);

printf("Q=\n");

  for(int i=0; i<n; i++){
    for(int j=0; j<m; j++){
      double q=gsl_matrix_get(A,i,j);
      printf("%lg ",q);
    }
    printf("\n");
  }
printf("\n \n");
printf("R=\n");

  for(int i=0; i<m; i++){
    for(int j=0; j<m; j++){
      double q=gsl_matrix_get(R,i,j);
      printf("%lg ",q);
    }
    printf("\n");
  }
printf("\n \n");

printf("QtQ= (should be 1)\n");

/*works only for 3*2 matrix A*/
double a =gsl_matrix_get(A,0,0);
double b= gsl_matrix_get(A,0,1);
double c=gsl_matrix_get(A,1,0);
double d=gsl_matrix_get(A,1,1);
double e=gsl_matrix_get(A,2,0);
double f=gsl_matrix_get(A,2,1);

double q1=a*a+c*c+e*e;
double q2=a*b+c*d+e*f;
double q3=q2;
double q4=b*b+d*d+f*f;

printf("%g %g\n",q1,q2);
printf("%g %g\n",q3,q4);
printf("\n \n");

double R1=gsl_matrix_get(R,0,0);
double R2=gsl_matrix_get(R,0,1);
double R3=gsl_matrix_get(R,1,0);
double R4=gsl_matrix_get(R,1,1);

double A1=R1*a+b*R3;
double A2=a*R2+b*R4;
double A3=R1*c+R3*d;
double A4=c*R2+d*R4;
double A5=R1*e+R3*f;
double A6=e*R2+f*R4;

printf("QR= (should be A)\n");

printf("%g %g\n",A1,A2);
printf("%g %g\n",A3,A4);
printf("%g %g\n",A5,A6);

printf("\n \n");

printf("Now, to test the back substitution:\n A=\n");

  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      double r = rand() %10; /*random double in range 0 to 9*/
      gsl_matrix_set(C,i,j,r);
      printf("%lg ",r);
    }
    printf("\n");
  }
  printf("\n \n");
  printf("b=\n");
  for(int i=0; i<N; i++){
    double r=rand() %10;
    gsl_vector_set(B,i,r);
    printf("%lg\n",r);
  }
printf("\n \n");

qr_gs_decomp(C,RC);
qr_gs_solve(C,RC,B,X);

/*printf("Q=\n");

  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      double q=gsl_matrix_get(C,i,j);
      printf("%lg ",q);
    }
    printf("\n");
  }
printf("\n \n");

printf("R=\n");

  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      double q=gsl_matrix_get(RC,i,j);
      printf("%lg ",q);
    }
    printf("\n");
  }
printf("\n \n");*/

printf("x=\n");
for(int i=0;i<N;i++) printf("%lg\n",gsl_vector_get(X,i));
printf("\n \n");

gsl_blas_dgemv(CblasNoTrans,1.0,RC,X,0.0,B); /*R*x -> b*/
gsl_blas_dgemv(CblasNoTrans,1.0,C,B,0.0,X); /*Q*(R*x) -> x*/

printf("QRx = (should be b)\n");

for(int i=0; i<N;i++) printf("%lg\n",gsl_vector_get(X,i));
printf("\n \n");

qr_gs_inverse(C,RC,INV);

printf("Now, checking the inverse function\n inv(A)=\n");

for(int i=0; i<N; i++){
  for(int j=0; j<N; j++){
    double q=gsl_matrix_get(INV,i,j);
    printf("%lg ",q);
  }
  printf("\n");
}

printf("\n \n");

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,RC,INV,0.0,D);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,C,D,0.0,INV);

printf("AB= (should be 1)\n");
for(int i=0; i<N; i++){
  for(int j=0; j<N; j++){
    double q=gsl_matrix_get(INV,i,j);
    printf("%lg ",q);
  }
  printf("\n");
}

gsl_matrix_free(INV);
gsl_vector_free(X);
gsl_vector_free(B);
gsl_matrix_free(RC);
gsl_matrix_free(C);
gsl_matrix_free(A);
gsl_matrix_free(R);

return 0;
}
