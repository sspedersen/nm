#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<lineq.h>
#include<math.h>

int newton (
	void f(gsl_vector* x,gsl_vector* fx),
	gsl_vector* x, double dx, double eps);

int newton_with_jacobian(
	void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
	gsl_vector* x, double dx, double eps);

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main() {
    gsl_vector* x=gsl_vector_alloc(2);
    gsl_vector* fx=gsl_vector_alloc(2);
		gsl_matrix* J=gsl_matrix_alloc(2,2);


// for the random function
  int ncalls=0;
	int ncallsj=0;
  void fran(gsl_vector *p, gsl_vector * fx){
    ncalls++;
    double A=1000;
    double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
    gsl_vector_set(fx,0,A*x*y-1);
    gsl_vector_set(fx,1,exp(-x)+exp(-y)-1.0-1.0/A);
  }

	void fran_jac(gsl_vector *p, gsl_vector * fx,gsl_matrix* J){
    ncallsj++;
    double A=1000;
    double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
    gsl_vector_set(fx,0,A*x*y-1);
    gsl_vector_set(fx,1,exp(-x)+exp(-y)-1.0-1.0/A);
		gsl_matrix_set(J,0,0,A*y);
		gsl_matrix_set(J,0,1,A*x);
		gsl_matrix_set(J,1,0,-exp(-x));
		gsl_matrix_set(J,1,1,-exp(-y));
  }

  gsl_vector_set(x,0,1);
  gsl_vector_set(x,1,0.5);
  printf("THE RANDOM FUNCTION:\nWithout given Jacobian:\n");
  vector_print("initial vector x: ",x);
	fran(x,fx);
	vector_print("            f(x): ",fx);
	int counts=newton(fran,x,1e-6,1e-3);
  vector_print("      solution x: ",x);
  fran(x,fx);
  vector_print("            f(x): ",fx);
	printf("ncalls = %i\n",ncalls);
	printf("counts= %i\n \n",counts);
	gsl_vector_set(x,0,1);
  gsl_vector_set(x,1,0.5);
	printf("With given Jacobian; same initial conditions \n");
	counts=newton_with_jacobian(fran_jac,x,1e-6,1e-3);
	vector_print("      solution x: ",x);
  fran_jac(x,fx,J);
  vector_print("            f(x): ",fx);
	printf("ncalls = %i\n",ncallsj);
	printf("counts = %i\n",counts);
  printf("\n\n");


//for the Rosenbrock function
  int ncallsr=0;
	void fros(gsl_vector* p,gsl_vector* fx){
		ncallsr++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
		gsl_vector_set(fx,1, 100*2*(y-x*x));
		}

	int ncallsrj=0;
	void fros_jac(gsl_vector* p, gsl_vector* fx, gsl_matrix* J){
		ncallsrj++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
		gsl_vector_set(fx,1, 100*2*(y-x*x));
		gsl_matrix_set(J,0,0,2-400*y+1200*x*x);
		gsl_matrix_set(J,0,1,-400*x);
		gsl_matrix_set(J,1,0,-400*x);
		gsl_matrix_set(J,1,1,200);
	}

	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,8);
	fros(x,fx);
	printf("ROSENBROCK'S FUNCTION:\nWithout given Jacobian\n");
	vector_print("initial vector x: ",x);
	vector_print("            f(x): ",fx);
	counts=newton(fros,x,1e-6,1e-3);
	//gsl_vector_fprintf(stderr,x,"%g");
	vector_print("      solution x: ",x);
	fros(x,fx);
	vector_print("            f(x): ",fx);
	printf("ncalls = %i\n",ncallsr);
	printf("counts = %i\n",counts);
  printf("\n\n");
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,8);
	printf("With Jacobian given; same initial vector x\n");
	counts=newton_with_jacobian(fros_jac,x,1e-6,1e-3);

	vector_print("      solution x: ",x);
	fros_jac(x,fx,J);
	vector_print("            f(x): ",fx);
	printf("ncalls = %i\n",ncallsrj);
	printf("counts= %i\n \n \n",counts);


// for the Himmelblau's function
	int ncallsh;
  void fhim(gsl_vector * p, gsl_vector * fx){
    ncallsh++;

    double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
    gsl_vector_set(fx,0,2*(x*x+y-11)*2*x+2*(x+y*y-7)    );
    gsl_vector_set(fx,1,2*(x*x+y-11)    +2*(x+y*y-7)*2*y);
  }

	int ncallshj;
	void fhim_jac(gsl_vector* p, gsl_vector* fx, gsl_matrix*J){
		ncallshj++;

		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
    gsl_vector_set(fx,0,2*(x*x+y-11)*2*x+2*(x+y*y-7)    );
    gsl_vector_set(fx,1,2*(x*x+y-11)    +2*(x+y*y-7)*2*y);
		gsl_matrix_set(J,0,0,4*(x*x+y-11)+8*x*x+2);
		gsl_matrix_set(J,0,1,4*x+4*y);
		gsl_matrix_set(J,1,0,4*x+4*y);
		gsl_matrix_set(J,1,1,4*(x+y*y-7)+8*y*y+2);
	}

  gsl_matrix * X=gsl_matrix_alloc(2,5); //allocating the 5 initial x's; based on wikipedia extrema
  gsl_matrix_set(X,0,0,-1);
  gsl_matrix_set(X,1,0,-1);
  gsl_matrix_set(X,0,1,5);
  gsl_matrix_set(X,1,1,5);
  gsl_matrix_set(X,0,2,-3);
  gsl_matrix_set(X,1,2,-3);
  gsl_matrix_set(X,0,3,-3);
  gsl_matrix_set(X,1,3,3);
  gsl_matrix_set(X,0,4,3);
  gsl_matrix_set(X,1,4,-2);

  printf("HIMMELBLAU'S FUNCTION:\n");
  for(int i=0; i<5;i++){
		ncallsh=0;
		ncallshj=0;
    gsl_vector_set(x,0,gsl_matrix_get(X,0,i));
    gsl_vector_set(x,1,gsl_matrix_get(X,1,i));
    printf("Extremum %i:\n Without given Jacobian:\n",i+1);
    vector_print("initial vector x:",x);
    fhim(x,fx);
    vector_print("            f(x):",fx);
    counts= newton(fhim,x,1e-6,1e-3);
    vector_print("   solution x: ",x);
    fhim(x,fx);
    vector_print("            f(x): ",fx);
		printf("ncalls=%i\n",ncallsh);
		printf("counts=%i\n \n",counts);
		printf("With given Jacobian:\n");
		gsl_vector_set(x,0,gsl_matrix_get(X,0,i));
    gsl_vector_set(x,1,gsl_matrix_get(X,1,i));
		counts=newton_with_jacobian(fhim_jac,x,1e-6,1e-3);
		vector_print("   solution x: ",x);
		fhim_jac(x,fx,J);
		vector_print("            f(x): ",fx);
		printf("ncalls=%i\n",ncallshj);
		printf("counts=%i\n \n \n",counts);

    printf("\n \n");
}

gsl_matrix_free(X);
gsl_vector_free(x);
gsl_vector_free(fx);
}
