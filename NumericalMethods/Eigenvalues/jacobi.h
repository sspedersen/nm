#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include <gsl/gsl_blas.h>
#include<assert.h>

int jacobi(gsl_matrix * A, gsl_vector * e, gsl_matrix * V);

int jacobi_lowest(gsl_matrix * A, gsl_vector * e, gsl_matrix * V, int numEV);

int jacobi_highest(gsl_matrix * A, gsl_vector * e, gsl_matrix * V, int numEV);
