#include"jacobi.h"
#include<stdlib.h>
#include<stdio.h>

void printsm(gsl_matrix * M){
  size_t n=M->size1;

  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      double m=gsl_matrix_get(M,i,j);
      printf("%lg ",m);
    }
    printf("\n");
  }
}

void printvec(gsl_vector * m){
  size_t n=m->size;
  for(int i=0;i<n;i++) printf("%lg \n",gsl_vector_get(m,i));
}


int main(int argc, char** argv){
  int n=3;
  if(argc>1) n=atoi(argv[1]);
  gsl_matrix * A= gsl_matrix_alloc(n,n);
  gsl_matrix * V= gsl_matrix_alloc(n,n);
  gsl_matrix * D= gsl_matrix_calloc(n,n);
  gsl_matrix * R= gsl_matrix_alloc(n,n);
  gsl_vector * e= gsl_vector_alloc(n);

  for(int i=0; i<n; i++){
    double di=rand() %10;
    gsl_matrix_set(A,i,i,di);

    if (i > 0) {
      for(int j=0; j<i; j++){
        double r=rand() %10;
        gsl_matrix_set(A,i,j,r);
        gsl_matrix_set(A,j,i,r);

      }
    }
  }

gsl_matrix* AA = gsl_matrix_alloc(n,n);
gsl_matrix * B =gsl_matrix_alloc(n,n);
gsl_matrix_memcpy(B,A);
gsl_matrix_memcpy(AA,A);

  jacobi(A,e,V);
  printf("A) Jacobi Diagonalization with cyclic sweeps:\n A is a symmetric matrix, D is the diagonal matrix corresponding to the eigenvalues of A, V contains the eigenvectors of A \n ");
  printf("A=\n");
  printsm(AA);
  printf("\n \n");

  //printf("sweeps=%i\n",sweeps1);
  //printf("\n \n");
  printf("After Jacobi Diagonalization:\n A=\n");
  printsm(A);
  printf("V=\n");
  printsm(V);
  printf("\n \n");

//gsl_matrix_set_identity(D);
  for(int i=0; i<n; i++) gsl_matrix_set(D,i,i,gsl_vector_get(e,i));
  printf("D=\n");
  printsm(D);
  printf("\n \n");

  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1.0,AA,V,0.0,R);
  gsl_blas_dgemm(CblasTrans, CblasNoTrans,1.0,V,R,0.0,D);

  printf("checking the algoritm works\n");
  printf("D = V^T A V =\n");
  printsm(D);
  printf("\n \n");
  printf("I was not able to record the time but have recorded number of rotations as a function of matrix size N, see compare.svg. The cyclic points are plotted and fitted to A*x^3\n");


  printf("\n \n");

// Part B:
gsl_matrix_memcpy(A,B);
gsl_matrix_memcpy(AA,B);
printf("B) Jacobi Diagonalization with eigenvalue-by-eigenvalue: \n Again, A = \n");
printsm(A);
printf("Jacobi lowest (phase=0), 3 (all) eigenvalues: \n e = \n");
jacobi_lowest(B,e,V,3);
printvec(e);
printf("\n \n");

printf("Jacobi lowest (phase=0), 1 eigenval: \n e = \n");
jacobi_lowest(A,e,V,1);
printvec(e);
printf("\n \n");

printf("Jacobi highest (phase = pi/2), 1 eigenvalue: \n e = \n");
jacobi_highest(AA,e,V,1);
printvec(e);
printf("\n \n");

printf("We see how jacobi_lowest (eigval = 3) returns all eigenvalues equal to the ones in A). When we set eigval=1 we get the smallest eigenvalue. \n");
printf("jacobi_highest does exactly the same but with the highest eigval first. \n");
printf("The angle phi is to be added with a phase: 0 for jacobi_lowest and pi/2 for jacobi_highest.\n");
printf("\n \n");

/*printf("Comparing number of rotations of cycles sweeps (all eigenval) and finding the lowest eigenval of A: \n ");
printf("CYC rot    = %i\n",sweeps1);
printf("LowEig rot = %i\n",sweeps2);
printf("\n \n");

printf("And comparing CYC rot with jacobi_lowest but finding all three eigenvalues: \n");
printf("CYC rot = %i\n",sweeps1);
printf("LowEif rot (all) = %i \n",sweeps3);*/

printf("#for comparison of cyclic sweeps and VBV: \n");
printf("# N sweeps_CYC sweeps_1 sweeps_full \n");
for (int N=10; N<500; N *= 1.70) {
    gsl_matrix * Atime = gsl_matrix_alloc(N,N);
    gsl_matrix * Vtime = gsl_matrix_alloc(N,N);
    gsl_vector * etime = gsl_vector_alloc(N);
  for(int i=0; i<N; i++){ //random symmetric matrix;
    double di=rand() %10;
    gsl_matrix_set(Atime,i,i,di);

    if (i > 0) {
      for(int j=0; j<i; j++){
        double r=rand() %10;
        gsl_matrix_set(Atime,i,j,r);
        gsl_matrix_set(Atime,j,i,r);

      }

    }
  }
  //printsm(Atime);
  int SweepsCYC=jacobi(Atime,etime,Vtime);
  int Sweeps1=jacobi_lowest(Atime,etime,Vtime,1);
  int Sweepsfull=jacobi_lowest(Atime,etime,Vtime,N);
  printf("%i %i %i %i\n",N,SweepsCYC,Sweeps1,Sweepsfull);

  gsl_matrix_free(Atime);
  gsl_matrix_free(Vtime);
  gsl_vector_free(etime);
}












  gsl_matrix_free(A);
  gsl_matrix_free(V);
  gsl_matrix_free(D);
  gsl_vector_free(e);
}
