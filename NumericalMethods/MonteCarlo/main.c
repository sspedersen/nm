#include<stdio.h>
#include<math.h>


void randomx(int dim, double *a, double *b, double *x);

void plainmc( int dim , double *a , double *b ,double f(double* x),int N, double *result , double *error);

int main(){

  // PART A:
  //simple integrals:
  int dim=3;
  double a[3]= {0, 0, 0};
  double b[3]= {1, M_PI, 2*M_PI};

  int N=1e5;
  double result, error;

  double f1(double *x){
    return sin(x[1])*x[0]*x[0]; }

  plainmc(dim,a,b,&f1,N,&result,&error);
  printf("A) TESTING PLAIN MC: \n");
  printf("i) The Volume of a Sphere with radius=1: Integrating 4*pi*r^2 \n");
  printf("MC_result= %g\n",result);
  printf("Exact = %g\n",4.0/3.0*M_PI);
  printf("\n \n");
  dim=1;
  double a1[1]= {0};
  double b1[1]= {1};

  double f2(double *x){
    return 1/sqrt(x[0]);
  }

  plainmc(dim,a1,b1,&f2,N,&result,&error);
  printf("ii) Integrating 1/sqrt(x) \n");
  printf("MC_result= %g\n",result);
  printf("Exact = %g\n",2.0);

  //some kegle
  double a2[1]= {0};
  double b2[1]= {10};
  double f3(double *x){
    return pow(2*(10.0-x[0])/10,2)*M_PI; }
  plainmc(dim,a2,b2,&f3,N,&result,&error);
  printf("iii) The Volume of a Cone with radius=2, height=10: Integrating (r*(h-x)/h)^2*pi\n");
  printf("MC_result= %g\n",result);
  printf("Exact = %g\n",1.0/3.0*2*2*10*M_PI);
  printf("\n \n");

  //difficult integral:
  double a3[3]={0, 0, 0};
  double b3[3]={M_PI, M_PI, M_PI};
  dim=3;
  double difint(double *x){
    return 1.0/(1-cos(x[0])*cos(x[1])*cos(x[2]));
  }

  plainmc(dim,a3,b3,&difint,N,&result,&error);
  printf("iv) The Difficult Integral! (1-cos(x)cos(y)cos(z))^-1 from 0 to pi: \n");
  printf("MC_result=%g\n",result);
  printf("Exact = %g \n",1.3932039296856768591842462603255);

  // PART B:
  int forloop=1e6;
  dim=3;
  printf("\n \n");
  printf("# N error result\n");
  for (int i=10; i<forloop; i*=10) {
    plainmc(dim,a,b,&f1,i,&result,&error);
    printf("%i %g %g\n",i,error,result);


  }


  return 0;
}
