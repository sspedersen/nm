A) TESTING PLAIN MC: 
i) The Volume of a Sphere with radius=1: Integrating 4*pi*r^2 
MC_result= 4.1705
Exact = 4.18879

 
ii) Integrating 1/sqrt(x) 
MC_result= 2.02956
Exact = 2
iii) The Volume of a Cone with radius=2, height=10: Integrating (r*(h-x)/h)^2*pi
MC_result= 41.9157
Exact = 41.8879

 
iv) The Difficult Integral! (1-cos(x)cos(y)cos(z))^-1 from 0 to pi: 
MC_result=43.9689
Exact = 1.3932 

 
# N error result
10 0.900175 3.88693
100 0.42833 3.78385
1000 0.144611 4.24009
10000 0.046118 4.18277
100000 0.0146214 4.19194
