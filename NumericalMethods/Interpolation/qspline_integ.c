#include"qspline.h"


double qspline_deriv(qspline* s, double z)
{
	assert(z>=s->x[0]);
	assert(z<=s->x[s->n-1]);
	int i=0, j=s->n-1;
	while (j-i >1){int m=( i+j )/2 ; if( z>=s->x[m]) i=m; else j=m;}
	double dx = z - s->x[i];
	return s->b[i] + 2*s->c[i]*dx;
}

double qspline_integ(qspline* s, double z)
{
	assert(z>=s->x[0]);
	int i=0, j=s->n-1; double dx=0, result=0;
	while (j-i >1){int m=( i+j )/2 ; if( z>s->x[m]) i=m; else j=m;}
	for (int u = 0; u < i; u++)
	{
		dx=s->x[u+1]-s->x[u];
		result+=dx*(s->y[u]+dx*(s->b[u]*1.0/2+1.0/3*dx*s->c[u]));
	}
	dx=z-s->x[i];
	result+=dx*(s->y[i]+dx*(s->b[i]*1.0/2+dx*s->c[i]*1.0/3));
	return result;
}
