#include"qspline.h"

double linterp(int, double*, double*, double);
double linterp_integ(int,double*,double*,double);

int main(){
/* this is for the linear interpolation part*/

  int i;
  int n=10;
  double x[n],y[n];
  printf("#x sin(x)\n");
  for (i = 0; i < 10; i=i+10/n) {
  x[i]=0.5+i;
  y[i]=sin(x[i]);
  printf("%lg %lg \n",x[i],y[i]);
}
  printf("\n \n");
  printf("#linterp of z\n");
  double j;
  for (j=x[0]; j<x[n-1]; j += 0.1) {
    double fj=linterp(n,x,y,j);
    printf("%lg %lg\n",j,fj);
  }

  printf("\n \n");
  printf("#actual sine\n");

  for(double i = 0; i< 10; i += 0.1){
    double sini = sin(i);
    printf("%lg %lg\n",i,sini);
  }

  printf("\n \n");
  printf("#linterp_integ for z=0 -> 10\n");

  for(double i=1; i< 10; i++){
    double Int=linterp_integ(n,x,y,i);
    printf("%lg %lg\n",i,Int);
  }
  printf("\n \n");
  printf("#actual -cosine\n");
  for(double i=0; i<10; i +=0.1){
    double mcosi=-cos(i);
    printf("%lg %lg\n",i,mcosi);
  }
printf("\n \n");

/* for the quadratic part */
printf("#quadratic spline of z\n");
qspline* Q=qspline_alloc(n,x,y);

for(double i=x[0]; i<=x[n-1]; i += 0.1){
  double fi=qspline_eval(Q,i);
  printf("%g %g\n",i,fi);
}

printf("\n \n");

printf("#quadratic integration\n");

for(double i=x[0]; i<x[n-1]; i++){
  double Int=qspline_integ(Q,i);
  printf("%g %g\n",i,Int);
}

printf("\n \n");

printf("#quadratic derivative\n");

for(double i=x[0]; i<x[n-1]; i++){
  double der=qspline_deriv(Q,i);
  printf("%g %g\n",i,der);
}


qspline_free(Q);
}
