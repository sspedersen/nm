#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

#ifndef HAVE_QSPLINE_H
#define HAVE_QSPLINE_H
typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline* qspline_alloc(int n,double* x,double* y);
double qspline_eval(qspline *s, double z);
double qspline_deriv(qspline *s, double z);
double qspline_integ(qspline *s, double z); 
void qspline_free(qspline *s);
#endif
