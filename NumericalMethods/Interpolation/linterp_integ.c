#include<assert.h>

double linterp_integ(int n, double *x, double *y, double z){
  assert (n>1 && z>=x[0] && z<=x[n-1]);
  int i=0, j=n-1;
  int m=0;
  while(j-i>1){
    m=(i+j)/2;
    if(z>x[m]) i = m;
    else j=m;
  }; /*finding the interval relevant for z */

  double xk, xkp,yk,ykp,bk,ak,int_k,int_acc;

  int k;
  for (k = 0; k < m; k++) {
    xk=x[k-1];
    xkp=x[k];
    yk=y[k-1];
    ykp=y[k];
    bk=(ykp-yk)/(xkp-xk);
    ak=yk-bk*xk;
    int_k=ak*(xkp-xk) + bk/2*(xkp*xkp-xk*xk);
    int_acc =0; int_acc += int_k; /*this should be the integral up to xi*/
  };
  double xz=x[m], xzp=x[m+1], yz=y[m], yzp=y[m+1];
  double bz=(yzp-yz)/(xzp-xz), az=yz-bz*xz;
  double int_z=az*(z-xz)+bz/2*(z*z-xz*xz);
  int_acc += int_z;

return int_acc;
}
