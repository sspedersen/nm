#include<gsl/gsl_vector.h>

double func(const gsl_vector *v, void *params){
  double x=gsl_vector_get(v,0);
  double f=0.5*(x*x-0.5*x);
  return f;
}
