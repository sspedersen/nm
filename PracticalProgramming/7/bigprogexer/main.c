#include<stdio.h>
#include<gsl/gsl_vector.h>

double func(const gsl_vector*,void*);
double minimum();

int main(){
  double xmin=minimum();
  fprintf(stderr,"The minimum is found at %lg",xmin);
  gsl_vector* v = gsl_vector_alloc(1);
  gsl_vector_set(v,0,xmin);
  double fmin=func(v,NULL);
  printf("%lg %lg\n",xmin,fmin);
  printf("\n \n");

  for (double x = 0; x<1; x+=0.05) {
    gsl_vector_set(v,0,x);
    double fx=func(v,NULL);
    printf("%lg %lg\n",x,fx);
  }
  gsl_vector_free(v);
  return 0;
}
