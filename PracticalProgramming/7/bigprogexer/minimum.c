#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

double func(const gsl_vector*,void*);

double minimum(){
  int dim=1;
  const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer* S= gsl_multimin_fminimizer_alloc(T,dim);

  gsl_multimin_function F;
  F.f = func;
  F.n=dim;
  F.params=NULL;

  gsl_vector *start= gsl_vector_alloc(dim), *STEP= gsl_vector_alloc(dim);
  gsl_vector_set(start, 0, 3);

  gsl_vector_set_all(STEP, 0.1);

  gsl_multimin_fminimizer_set(S, &F, start, STEP);

  double x= gsl_vector_get( S -> x,0);

  int flag; int iteration=0; double size;
  do{
    gsl_multimin_fminimizer_iterate(S);
    size = gsl_multimin_fminimizer_size(S);
    flag = gsl_multimin_test_size(size, 1e-3);
    iteration++;
    x = gsl_vector_get(S -> x, 0);
    fprintf(stderr,"iteration = %i x= %g\n", iteration,x);
  }while(flag==GSL_CONTINUE);

  gsl_multimin_fminimizer_free(S);
  gsl_vector_free(start);
  gsl_vector_free(STEP);

  return x;
}
