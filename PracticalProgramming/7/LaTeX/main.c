#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<math.h>

double integrand(double x, void* params){
return 2/(sqrt(M_PI))*exp(-x*x);
}

double integrate(double x){
  gsl_function f;
  f.function = integrand;
  f.params=NULL;

  size_t limit=100;
  gsl_integration_workspace* workspace=gsl_integration_workspace_alloc(limit);

  double acc=1e-6, eps=1e-6, result, err;
  int key=4;

  gsl_integration_qag(&f,0,x,acc,eps,limit,key,workspace,&result,&err);

  return result;
}

double taylor4(double x){
  return 2/sqrt(M_PI)*(x-pow(x,3)/3+pow(x,5)/10 - pow(x,7)/42);
}

int main(int argc, char** argv){
  double a=-3;
  if(argc>1) a=atof(argv[1]);
  double b=3;
  if(argc>2) b=atof(argv[2]);
  double dx=0.1;
  if(argc>3) dx=atof(argv[3]);


  for (double x=a; x<b+dx; x+=dx){
    printf("%g %g \n",x,integrate(x));
  }
  printf("\n \n");


  for(double x=a; x<b+dx; x+=dx){
    printf("%g %g \n",x, taylor4(x));
  }
return 0;
}
