#ifndef HAVE_KOMPLEX_H /*necessary for multiple includes*/

struct komplex {double re; double im;};
typedef struct komplex komplex;

void komplex_print  (char* s, komplex x); /*prints string s and complex x */
void komplex_set  (komplex* z, double x, double y); /*z=x+iy */
komplex komplex_new (double x, double y); /*return x+iy*/
komplex komplex_add      (komplex a, komplex b); /* returns a+b */
komplex komplex_sub      (komplex a, komplex b); /* returns a-b */

#define HAVE_KOMPLEX_H
#endif
