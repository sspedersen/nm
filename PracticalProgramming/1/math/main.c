#include <stdio.h>
#include <math.h>
#include <complex.h>

int main(void)
{
  printf("A) Various calculations using math.h, complex.h\n");
float n=5;
 printf("Gamma(5)=%f\n",tgamma(n)); /*gamma value for 5*/

double m=0.5;
printf("Bessel(0.5)=%g\n",j1(m));

double l=-2;
double complex z=csqrt(l);
printf("sqrt2=%g%+gi\n",creal(z),cimag(z));

double complex x=cexp(I);
printf("e^i=%g%+gi\n",creal(x),cimag(x));

double complex y=cexp(I*M_PI);
printf("e^ipi=%g%+gi\n",creal(y),cimag(y));

double complex a=cpow(I,M_E);
printf("i^e=%g%+gi\n",creal(a),cimag(a));

printf("\n \n");
printf("B) Number of significant digits for different types of variables:\n number =  0.1111111111111111111111111111\n");

float f= 0.1111111111111111111111111111 ;
printf("Float: number = %.25g\n",f);

double d=  0.1111111111111111111111111111;
printf("Double: number = %.25lg \n",d);

long double ld =  0.1111111111111111111111111111L;
printf("Long double: number = %.25Lg\n",ld);


return 0;
}
