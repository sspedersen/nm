#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>

int main(){
  printf("Solving the linear eq. system: A x = b\n");
  int n=3;
  gsl_matrix* M= gsl_matrix_alloc(n,n);
  gsl_matrix* Mcopy= gsl_matrix_alloc(n,n);

  gsl_matrix_set(M,0,0,6.13);
  gsl_matrix_set(M,0,1,-2.90);
  gsl_matrix_set(M,0,2,5.86);
  gsl_matrix_set(M,1,0,8.08);
  gsl_matrix_set(M,1,1,-6.31);
  gsl_matrix_set(M,1,2,-3.89);
  gsl_matrix_set(M,2,0,-4.36);
  gsl_matrix_set(M,2,1,1.00);
  gsl_matrix_set(M,2,2,0.19);
printf("M=\n");
for(int j=0; j<M->size2;j++){
  for(int i=0; i<M->size1;i++)
    printf("%g ",gsl_matrix_get(M,i,j));
  printf("\n");
};
  gsl_matrix_memcpy(Mcopy,M);
  int m=3;
  printf("\n \n");
  gsl_vector* b= gsl_vector_alloc(m);
  gsl_vector_set(b,0,6.23);
  gsl_vector_set(b,1,5.37);
  gsl_vector_set(b,2,2.29);

printf("b=\n");
for(int k=0; k<b->size; k++){
  printf("%g\n",gsl_vector_get(b,k));
};
printf("\n \n");
gsl_vector* x= gsl_vector_alloc(3);
gsl_linalg_HH_solve(M,b,x);
printf("x=\n");
for(int l=0; l<x->size; l++)
printf("%g\n",gsl_vector_get(x,l));
printf("\n \n");

//checking the result:
gsl_blas_dgemv(CblasNoTrans,1,M,x,0,b);
printf("M*x = (should be b)\n");
for(int i=0; i<3; i++) printf("%g\n",gsl_vector_get(x,i));

gsl_vector_free(x);
gsl_matrix_free(M);
gsl_vector_free(b);
}
