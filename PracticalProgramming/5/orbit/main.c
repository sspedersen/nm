#include<stdio.h>
#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int almen_ode(double x, const double y[], double dydx[], void* params){
  dydx[0]=y[0]*(1.0-y[0]);
  return GSL_SUCCESS;

}
double logfun(double x){
  double f=1.0/(1.0+exp(-x));
  return f;
}
double solve_almen_ode(double x){
  gsl_odeiv2_system sys;
  sys.function=almen_ode;
  sys.jacobian=NULL;
  sys.dimension=1;
  sys.params=NULL;

  double hstart=copysign(0.1,x);
  double acc=1e-6;
  double eps=1e-6;
  gsl_odeiv2_driver* driver =
  		gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

  double t=0;
  double y[1]={0.5};
  gsl_odeiv2_driver_apply(driver,&t,x,y);

  gsl_odeiv2_driver_free(driver);
  return y[0];
}
/*for the orbit part*/

int orbit_ode(double phi, const double y[], double dydx[], void *params)
{
double epsilon = *(double *) params;
dydx[0]=y[1];
dydx[1]=1-y[0] + epsilon*y[0]*y[0];
return GSL_SUCCESS;
}

double solve_orbit(double x, double epsilon,double u0, double up0){
gsl_odeiv2_system sys;
sys.function=orbit_ode;
sys.jacobian= NULL;
sys.dimension=2;
sys.params=(void *) &epsilon;

double hstart=copysign(0.1,x);
double acc= 1e-6;
double eps=1e-6;
gsl_odeiv2_driver* driver =
    gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

double t=0;
double y[2]={u0, up0};
gsl_odeiv2_driver_apply(driver,&t,x,y);
gsl_odeiv2_driver_free(driver);
return y[0];
}
/*and the main function*/
int main(){
  for(double x=0;x<3;x+=0.1)
  printf("%g %g \n",x,solve_almen_ode(x));
  printf("\n \n");

for (double i = 0; i < 3.0; i+=0.05) {
  printf("%g %g \n",i,logfun(i));}
 printf("\n \n");

 for (double j=0; j < 3*M_PI; j+=+0.1) {
   printf("%g %g \n",j,solve_orbit(j,0,1,0));
 }
printf("\n \n");

for(double k=0; k< 10*M_PI; k+=0.1){
  printf("%g %g\n",k,solve_orbit(k,0,1,-0.5));
}

printf("\n \n");

for(double l=0; l<10*M_PI; l+=0.1){
  printf("%g %g\n",l,solve_orbit(l,0.01,1,-0.5));
}

  return 0; }
