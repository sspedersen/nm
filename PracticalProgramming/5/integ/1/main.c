#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<math.h>

double integrand(double x, void* params){
  return log(x)/sqrt(x);
}

int main(){
  gsl_function f;
  f.function=integrand;
  f.params=NULL;

  int limit=100;
  gsl_integration_workspace* workspace=gsl_integration_workspace_alloc(limit);

double a=0;
double b=1;
  double acc=1e-6, eps=1e-6, result, err;
  gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result, &err);
printf("1) Integrating log(x)/sqrt(x) from 0 to 1:\n");
printf("Integral=%g (should be 1)\n",result);

  return 0;
}
