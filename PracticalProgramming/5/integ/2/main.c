#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<math.h>

double integrand1(double x, void* params){
double alpha=*(double*) params;
return exp(-alpha*pow(x,2)); /* ^ = ** */
}

double integrand2(double x, void* params){
  double alpha=*(double*) params;
  return (-pow(alpha,2)*pow(x,2)/2 + alpha/2 + pow(x,2)/2)*exp(-alpha*pow(x,2));
}


double integrate(double alpha){
  gsl_function f1, f2;
  f1.function=integrand1;
  f2.function=integrand2;
  f1.params=(void *) &alpha;
  f2.params=(void *) &alpha;

  size_t limit=100;
    gsl_integration_workspace* workspace=gsl_integration_workspace_alloc(limit);

  double acc=1e-6, eps=1e-6, result1,result2, err;
  gsl_integration_qagi(&f1,acc,eps,limit,workspace,&result1,&err);
  gsl_integration_qagi(&f2,acc,eps,limit,workspace,&result2,&err);
  double Ealpha=result2/result1;
  return Ealpha;
}


int main(){
  for (double x = 0.5; x < 1.5; x+=0.01) {
    printf("%g %g\n",x,integrate(x));
  }
}
