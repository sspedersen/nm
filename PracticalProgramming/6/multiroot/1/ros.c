#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>
#include<stdio.h>
int rosenbrock(const gsl_vector* v, void* params, gsl_vector* f){

  double x= gsl_vector_get(v,0);
  double y= gsl_vector_get(v,1);
  double f0= -2*(1-x)+2*100*(y-x*x)*(-2*x);
  double f1= 2*100*(y-x*x);
  gsl_vector_set(f,0,f0);
  gsl_vector_set(f,1,f1);

  return GSL_SUCCESS;
}


int main(){

int dim=2;
const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,dim);

gsl_multiroot_function F;
F.f=rosenbrock;
F.n=dim;
F.params=NULL;

gsl_vector* start = gsl_vector_alloc(dim);
gsl_vector_set(start,0,2.0);
gsl_vector_set(start,1,3.0);
gsl_multiroot_fsolver_set(S,&F,start);

double x=gsl_vector_get(S->x,0);
double y=gsl_vector_get(S->x,1);

int flag; int iteration=0;
do{
gsl_multiroot_fsolver_iterate(S);
flag = gsl_multiroot_test_residual(S->f,1e-12);
iteration += 1;
x=gsl_vector_get(S->x,0);
y=gsl_vector_get(S->x,1);

printf("%g %g %g\n",x,y,pow(1-x,2)+100*pow(y-x*x,2));

}while(flag==GSL_CONTINUE);

printf("\n \n");

gsl_vector_free(start);
gsl_multiroot_fsolver_free(S);
printf("the minimum is found at\n");
printf("x= %g, y= %g\n",x,y);
printf("number of iterations=%i\n",iteration);
}
