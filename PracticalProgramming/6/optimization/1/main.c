#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>

double ros(const gsl_vector *v, void *params){
  double x=gsl_vector_get(v,0);
  double y=gsl_vector_get(v,1);
  double f=pow(1-x*x,2) + 100*pow(y-x*x,2);
  return f;
}

int main(){
int dim = 2;
const gsl_multimin_fminimizer_type* T= gsl_multimin_fminimizer_nmsimplex2;
gsl_multimin_fminimizer* S= gsl_multimin_fminimizer_alloc(T,dim);

gsl_multimin_function F;
F.f=ros;
F.n=dim;
F.params=NULL;

gsl_vector *start = gsl_vector_alloc(dim), *STEP= gsl_vector_alloc(dim);
gsl_vector_set(start, 0, 3); gsl_vector_set(start,1,5); /*starting at x=3, y=5 */
gsl_vector_set_all(STEP, 1.0);

gsl_multimin_fminimizer_set(S, &F, start, STEP);

double x = gsl_vector_get(S -> x,0), y = gsl_vector_get(S -> x,1);

int flag; int iteration = 0; double size;
do{
  gsl_multimin_fminimizer_iterate(S);
  size = gsl_multimin_fminimizer_size(S);
  flag = gsl_multimin_test_size(size, 1e-12);
  iteration++;
  x=gsl_vector_get(S ->x,0); y=gsl_vector_get(S -> x,1);
  printf("iteration = %i, x= %g, y= %g\n",iteration,x,y);
}while(flag==GSL_CONTINUE);

printf("minimum found at x=%g and y=%g\n",x,y);

gsl_multimin_fminimizer_free(S);
gsl_vector_free(start);
gsl_vector_free(STEP);

}
