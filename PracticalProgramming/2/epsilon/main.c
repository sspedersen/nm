#include<stdio.h>
#include<limits.h>
#include<math.h>
#include<float.h>

int equal(double a, double b, double tau, double epsilon);

int main(){
  printf("1) On the limits of limits.h, float.h \n");
  int i=INT_MAX-1000;
  while (i+1>i) {
	i++;
  }

  int j;
  for (j=INT_MAX-1000; j+1>j; j++){
  };

  int k=INT_MAX-1000;
  do {
    k++;
  } while(k<k+1);

  printf("INT_MAX(WHILE) = %i\n",i);
  printf("INT_MAX(FOR) = %i\n",j);
  printf("INT_MAX(DO-WHILE)= %i\n",k);
  printf("INT_MAX = %i\n",INT_MAX);

  printf("\n \n");

  i=INT_MIN+1000;
  while (i-1<i) {
  i--;
  }

  for (j=INT_MIN+1000; j-1<j; j--){
  };

  k=INT_MIN+1000;
  do {
    k--;
  } while(k>k-1);

  printf("INT_MIN(WHILE) = %i\n",i);
  printf("INT_MIN(FOR) = %i\n",j);
  printf("INT_MIN(DO-WHILE)= %i\n",k);
  printf("INT_MIN = %i\n",INT_MIN);

  double d=1; while(1+d!=1){d/=2;} d*=2;
  float f=1; while(1+f!=1){f/=2;} f*=2;
  long double ld=1; while(1+ld!=1){ld/=2;} ld*=2;


  printf("\n \n");
  printf("Machine epsilon (ME): \n");
  printf("ME double =  %lg\n",d);
  printf("DBL_EPSILON = %lg\n",DBL_EPSILON);
  printf("ME float =  %lg\n",f);
  printf("DBL_FLOAT = %lg\n",FLT_EPSILON);
  printf("ME long double =  %Lg\n",ld);
  printf("DBL_LDBL = %Lg\n",LDBL_EPSILON);

//part 2;
  int max=INT_MAX/4;
  float sum_up_float = 0;
  for (int i=1; i<=max; i++) {
    sum_up_float += 1.0f/i;
  }
  float sum_down_float = 0;
  for(int i=max; i>=1; i--){
    sum_down_float += 1.0f/i;
  }

  double sum_up_double = 0;
  for(int i=1; i<=max; i++){
    sum_up_double += 1.0f/i;
  }

  double sum_down_double = 0;
  for(int i=max; i>=1; i--){
    sum_down_double += 1.0f/i;
  }

  printf("\n \n");
  printf("2) Summing up and down with different variables:\n");
  printf("Float:\nSum up = %lg\n",sum_up_float);
  printf("Sum down = %lg\n",sum_down_float);
  printf("\n");
  printf("Double:\nSum up = %lg\n",sum_up_double);
  printf("Sum down = %lg\n",sum_down_double);
  printf("\n");
  printf("The equal function: \n");
  double a=4.5; double b=4.501; double tau=0.01; double epsilon=0.1;
  int m = equal(a,b,tau,epsilon);
  printf("is %lg == %lg with tau = %lg, epsilon = %lg?\n return %i\n",a,b,tau,epsilon,m);
  return 0;
}
