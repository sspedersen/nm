#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <float.h>

int main(){
int i=1;
/*while(i+1>i){i++;}
printf("my max int=%i\n",i);
printf("lh max int=%i\n",INT_MAX);

int j;
for(j=1; j+1>j; j++);
printf("mmi med for=%i\n",j);

int k=1; 
while(k-1<k){k=k-1;}
printf("my min int=%i\n",k);
printf("lh min int=%i\n",INT_MIN);*/

double x=1;
while(1+x!=1){x/=2;} x*=2; /*/= means x=x/2. *= means x=x/2*/
printf("double=%g\n",x);
printf("double inbuilt=%g\n",DBL_EPSILON);

float y=1;
while(1+y!=1){y/=2;} y*=2; 
printf("float=%f\n",y);
printf("float inbuilt=%f\n",FLT_EPSILON);

long double z=1;
while(1+z!=1){z/=2;} z*=2;
printf("long double=%lg\n",z);
printf("ld inbuilt=%lg\n",LDBL_EPSILON);
return 0; 

}
