#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <float.h>

int main(){
int max=10000000; /*INT_MAX/3;*/ 
printf("max=%i\n",max);
printf("intmax=%i\n",INT_MAX);
float i=1; 
float sum_up=0;
while(i <= max){sum_up += 1.0/i; i++;}
printf("sumupf=%f\n",sum_up); 


/*the second sum*/
float j=1;
float sum_down=0;
while(j <= max){sum_down += 1.0/(max+1-j); j++;}
printf("sumdownf=%f\n",sum_down);


/* and for double, the same calculation */
double k=1; 
double sumupd=0;
while(k <= max){sumupd += 1.0/k; k++;}
printf("sumupd=%g\n",sumupd); 

double l=1; 
double sumdownd=0; 
while(l <= max){sumdownd +=1.0/(max+1-l);l++;}
printf("sumdownd=%g\n",sumdownd);

return 0;
}
